<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomerTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::add([
            'name' => 'Dinda Ariyani',
            'phone' => '082211927398',
            'address' => 'Ciluar Permai Bogor',
            'email' => 'ariyanid198@gmail.com'
        ]);

        Customer::add([
            'name' => 'Dian Ayu',
            'phone' => '081256324599',
            'address' => 'Perum Tatya Asri',
            'email' => 'ayudian3026@gmail.com'
        ]);
    }
}